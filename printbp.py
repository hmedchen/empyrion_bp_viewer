#!/usr/bin/env python3
import sys
import os
import io
import zipfile

import printbp_config as config
import codecs
from operator import pos

#------- CONSTANTS ----------
X=0
Y=1
Z=2

#------- CLASSES -------------
class Blueprint:
  filename=''
  content=b''
  zipstart=-1
  size = [0,0,0]

  def __init__ (self, filename):

    # save filename base
    self.filename_base=filename[:-4]
    # open bp file to write blueprint file
    self.filename=filename

    # read file in
    with open(self.filename,'rb') as fp:
      self.content = fp.read()
    # Attempt to find the zip section by searching backwards for the 0x0304140000000800 string.
    self.zipstart = self.content.rfind(config.ZIP_START_IN_FILE)
    self.zipblock = config.ZIP_START_BYTE + self.content[self.zipstart:]

    # unzip section
    f = zipfile.ZipFile(io.BytesIO(self.zipblock),'r')

    # Confirm that the zip pases self-checks.
    if f.testzip() is not None:
      print ("Zip failed to pass self-test. exitting")
      exit(1)

    # header data
    self.header=Header(self.content[:self.zipstart])
    self.size[X]=int.from_bytes(self.header.get_pos(config.V18_SIZEX_POS,4),byteorder="little")
    self.size[Y]=int.from_bytes(self.header.get_pos(config.V18_SIZEY_POS,4),byteorder="little")
    self.size[Z]=int.from_bytes(self.header.get_pos(config.V18_SIZEZ_POS,4),byteorder="little")
    self.num_blocks=self.size[X]*self.size[Y]*self.size[Z]

    # unzipped blueprint data
    self.blocks=Blocks(f.read('0'), self.size, self.num_blocks, self.filename_base)


  def get_version(self):
    return self.header.get_version()


  def get_size(self):
    if self.header.get_version() in [17,18]:
      return(self.size)



class Header:
  header_data=b''
  version=0
  def __init__ (self, header):
    self.header_data=header

    if self.header_data.index(config.HEADER_VALIDITY_CHECK_REF,config.HEADER_VALIDITY_CHECK_POS) != 0:
      print("file does not seem to be a valid Empyrion blueprint! Exiting")
      sys.exit(2)

    self.version = self.header_data[config.MARKER_HEADER_FILE_VERSION_POS]

  def print(self):
    print (self.header_data)

  def get_version(self):
    return (self.version)

  def is_version_supported(self):
    if self.get_version() in config.SUPPORTED_VERSIONS:
      return (True)
    else:
      return (False)

  def get_pos(self,pos,len=1):
    return (self.header_data[pos:(pos+len)])

  def get_creator (self):
    #get steam ID length
    sid_len=int.from_bytes(self.get_pos(config.HEADER_STEAMID_LEN_POS),byteorder=sys.byteorder)
    #get steam ID
    sid=self.get_pos(config.HEADER_STEAMID_POS,sid_len).decode("utf-8")
    #calc steam username length position (offset after Steam ID)
    susr_pos=config.HEADER_STEAMID_POS+sid_len+config.HEADER_STEAMNAME_LEN_OFFSET
    #get steam username length
    susr_len=int.from_bytes(self.get_pos(susr_pos),byteorder=sys.byteorder)
    #get steam username
    susr=self.get_pos(susr_pos+config.HEADER_STEAMNAME_OFFSET,susr_len).decode("utf-8")
    #return username, userid
    return ([susr,sid])

class Blocks:
  allblocks=b''
  filename=''
  def __init__ (self, blocks, size, num_blocks, filename_base):
    self.allblocks=blocks
    self.filename=filename_base + config.EXT_UNZIPPED
    self.size=size
    self.num_blocks=num_blocks

  def write_blocks_file (self):
    # open bin file and write blueprint file
    with open(self.filename,"wb") as outf:
       outf.write(self.allblocks)

  def read_blocks (self):
    self.bitmap_size=int.from_bytes(self.allblocks[0:4],byteorder='little')
    self.bitmap=""
    self.block_size=[0,0,0]

    print ("bitmap size (bytes/bits): {}/{} Calculated Numbers of blocks: {}".format(self.bitmap_size,self.bitmap_size*8, self.num_blocks))

#    start=4
    # bytewise from start to end
    for x in range(config.BITMAP_START,int(self.bitmap_size)+config.BITMAP_START):
      #little endian bit string 
      bit_rep=bin(int.from_bytes(self.allblocks[x:x+1],byteorder='little')).lstrip('0b').zfill(8)[::-1]
      self.bitmap+=bit_rep
    #cut off end
    self.bitmap=self.bitmap[:self.num_blocks]

    # Z,Y,X seems to be the correct order, but WHY are we 5 bits short per line?
    A=Z
    B=Y
    C=X

    i=0
    # init blocks
#    self.blocks=[[[-1 for c in range(self.size[C]) ] for b in range(self.size[B])] for a in range(self.size[A])]
    ## empty block list
    self.blocks=[]
    for a in range(self.size[A]):
      # add list for each level
      self.blocks.append([])
      for b in range(self.size[B]):
        #add lisst for each row
        self.blocks[a].append([])
        for c in range (self.size[C]):
          #add block
#          self.blocks[a][b].append(self.bitmap[i])
          self.blocks[a][b].append(Block([a,b,c],self.bitmap[i],i,self.bitmap_size))
#          print ("->",a,b,c,i)
#          self.blocks[a][b][c]=self.bitmap[i]
          i+=1
    self.block_size[X]=self.size[A]
    self.block_size[Y]=self.size[B]
    self.block_size[Z]=self.size[C]
    print (len(self.blocks),len(self.blocks[0]),len(self.blocks[0][0]))
    print (self.block_size)
          
  def print_all_blocks2 (self,C=X,B=Y,A=Z):
    n=0      
    for a in range (self.block_size[A]):
       for b in range (self.block_size[B]):
          print ("{:4}".format(n),end=" | ")
          for c in range (self.block_size[C]):
            self.print_block(a,b,c)
            n+=1
          print ("|")
       print ("     ",end='')
       for i in range(c+3):
         print ("- ",end='')
       print ("")

  def print_all_blocks (self,C=X,B=Y,A=Z):
    n=0      
    for z in range (self.block_size[Z]):
       for y in range (self.block_size[Y]):
          print ("{:4}".format(n),end=" | ")
          for x in range (self.block_size[X]):
            self.print_block(x,y,z)
            n+=1
          print ("|")
       print ("     ",end='')
       for i in range(x+3):
         print ("- ",end='')
       print ("")

  def print_block(self,x,y,z):
 #   print (x,y,z)
    try:
      print (self.blocks[x][y][z].get_bit(),end=" ")
    except IndexError as e:
      print ("\nIndexError on [{}][{}][{}] - no such field!".format(x,y,z))
      sys.exit(5)

class Block:
  pos=[0,0,0]
  blockinfo=b''
  bit=0
  
  def __init__ (self,pos,bit,blocknum,bitfield_size):
    self.pos=pos
    self.bit=int(bit)
    self.blocknum=blocknum 

  def get_pos(self):
    return (self.pos)
 
  def get_bit(self):
    return (self.bit)

#----------------
#  MAIN
#----------------
def main(argv):

  if len(sys.argv) > 1:
    filename = sys.argv[1]
  else:
    filename="examples/blocktest.epb"

  if filename[-4:] != config.EXT_BP:
    print ("Wrong file extension. Please use a {} file".format(config.EXT_BP))
    sys.exit(1)

  # init blueprint
  bp = Blueprint(filename)
  # write out blocks section to a file
  bp.blocks.write_blocks_file()

  print("BP Version: {} - Supported: {}".format(bp.get_version(), bp.header.is_version_supported()))
  if bp.header.is_version_supported() == False:
    print ("Blueprint version is not supported. Exitting.")
    sys.exit(3)
  #bp.header.print()
  print ("BP dimensions: {}".format(bp.get_size()))
  print ("BP creator: {}".format(bp.header.get_creator()))

  bp.blocks.read_blocks()
  #-------- no idea yet why only the x-y-z sequence works
  bp.blocks.print_all_blocks(X,Y,Z)

if __name__ == "__main__":
    main(sys.argv)


